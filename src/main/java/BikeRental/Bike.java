package BikeRental;

import java.util.Date;

public class Bike {

    private String name;
    private String brand;
    private String color;
    private Integer price;
    private Boolean available;
    private Date rentDate;


    public Bike(String name, String brand, String color, Boolean available, Integer price) {
        this.name = name;
        this.brand = brand;
        this.color = color;
        this.price = price;
        this.available = available;
    }

    public void rent() {
        available = false;
        rentDate = new Date();
    }

    public Date getRentDate() {
        return rentDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", available=" + available +
                ", rentDate=" + rentDate +
                '}';
    }
}
