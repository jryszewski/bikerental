package BikeRental;

import java.util.*;

public class BikeRental {

    Scanner scanner = new Scanner(System.in);
    List<Bike> bikes;
    List<Bike> mybikes = new ArrayList();
    int money;

    void begin() {

        System.out.println("Podaj ile masz w portfelu: ");
        money = Integer.parseInt(scanner.nextLine());
        BikeData bikedata = new BikeData();
        bikes = bikedata.getBikes();
        displayBike(bikes);
        instructions();
        nextCommand();
        System.out.println("Do widzenia !!!");
    }

    void instructions() {
        System.out.println("Dostępne komendy: \n " +
                "- filtruj cena/kolor/brand/status(true/false) np. filtruj status true \n " +
                "- zarezerwuj np. zarezerwuj 12\n " +
                "- moje rowery - wyswietli moje zarezerwowane rowery\n " +
                "- wypisz - wypisze wszystkie rowery\n " +
                "- doladuj np. doladuj 20 \n " +
                "- koniec \n ");
    }

    void nextCommand() {

        System.out.println("Co chcesz zrobic ? ");
        String command = scanner.nextLine();
        String[] commandArray = command.split(" ");

        if (command.equalsIgnoreCase("koniec")) {
            System.out.println("Podsumowanie: ");
            Date actualDate = new Date();
            for (Bike bike : mybikes) {
                float seconds = differenceInSeconds(bike.getRentDate(), actualDate);
                float price = seconds / 10 * bike.getPrice();
                System.out.println(bike);
                System.out.println("Ten rower miales: " + seconds / 10 + " godziny" + " i kosztował: " + price + " PLN");
                money -= price;
                bike.setAvailable(true);
            }

            mybikes = new ArrayList();

            System.out.println("Zostało Ci: " + money + " PLN");
            if (money >= 0) {
                return;
            } else {
                System.out.println("Brak środków, doładuj konto ");
            }
        }
        if (commandArray[0].equalsIgnoreCase("zarezerwuj")) {
            int i = Integer.parseInt(commandArray[1]);
            Bike bike = bikes.get(i - 1);
            if (bike.getAvailable() == false) {
                System.out.println("Nie udało sie zarezerwować rower");
            } else {
                bike.rent();
                mybikes.add(bike);
                System.out.println("Udało sie zarezerwować rower");
            }
        }
        if (commandArray[0].equalsIgnoreCase("wypisz")) {
            displayBike(bikes);
        }
        if (command.equalsIgnoreCase("moje rowery")) {
            displayBike(mybikes);
        }
        if (commandArray[0].equalsIgnoreCase("filtruj")) {
            if (commandArray[1].equals("kolor")) {
                displayBikebyColor(bikes, commandArray[2]);
            }
            if (commandArray[1].equalsIgnoreCase("cena")) {
                displayBikeByPrice(bikes);
            }
            if (commandArray[1].equalsIgnoreCase("brand")) {
                displayBikeByBrand(bikes, commandArray[2]);
            }
            if (commandArray[1].equalsIgnoreCase("status")) {
                displayBikeByStatus(bikes, commandArray[2]);
            }
        }
        if (commandArray[0].equalsIgnoreCase("doladuj")) {
            money += Integer.parseInt(commandArray[1]);
            System.out.println("Udało Ci się zasilić konto, Twój obecny stan konta: " + money + " PLN");
        }
        nextCommand();
    }

    void displayBike(List<Bike> bikes) {
        for (int i = 0; i < bikes.size(); i++) {
            System.out.println((i + 1) + ": " + bikes.get(i).toString());
        }
    }

    void displayBikebyColor(List<Bike> bikes, String color) {
        for (int i = 0; i < bikes.size(); i++) {
            if (bikes.get(i).getColor().equals(color)) {
                System.out.println((i + 1) + ": " + bikes.get(i).toString());
            }
        }
    }

    void displayBikeByBrand(List<Bike> bikes, String brand) {
        for (int i = 0; i < bikes.size(); i++) {
            if (bikes.get(i).getBrand().equals(brand)) {
                System.out.println((i + 1) + ": " + bikes.get(i).toString());
            }
        }
    }

    void displayBikeByStatus(List<Bike> bikes, String status) {
        for (int i = 0; i < bikes.size(); i++) {
            if (bikes.get(i).getAvailable().toString().equals(status)) {
                System.out.println((i + 1) + ": " + bikes.get(i).toString());
            }
        }
    }

    void displayBikeByPrice(List<Bike> bikes) {
        bikes.sort(Comparator.comparing(Bike::getPrice));
        for (int i = 0; i < bikes.size(); i++) {
            System.out.println((i + 1) + ": " + bikes.get(i).toString());
        }
    }

    float differenceInSeconds(Date d1, Date d2) {
        return (d2.getTime() - d1.getTime()) / 1000; //gettime podaje liczbe milisekund liczac od jakiejs daty
    }
}


