package BikeRental;

import java.util.List;

import static java.util.Arrays.asList;


public class BikeData {

    public List<Bike> getBikes() {

        List<Bike> bikes = asList(
                new Bike("Bike1", "BMX", "czerwony", false, 2),
                new Bike("Bike1", "BMX", "czerwony", true, 2),
                new Bike("Bike1", "BMX", "czerwony", false, 2),
                new Bike("Bike2", "Romet", "czarny", false, 8),
                new Bike("Bike2", "Romet", "czarny", true, 8),
                new Bike("Bike3", "Kands", "zielonu", true, 5),
                new Bike("Bike4", "Kross", "rożowy", true, 11),
                new Bike("Bike5", "Btwin", "czerwony", false, 3),
                new Bike("Bike6", "Giant", "zielony", false, 15),
                new Bike("Bike7", "Giant", "czerwony", false, 4),
                new Bike("Bike8", "BMX", "czarny", true, 6),
                new Bike("Bike9", "BMX", "czerwony", true, 1),
                new Bike("Bike10", "BMX", "czarny", true, 2)
        );
        return bikes;

    }
}